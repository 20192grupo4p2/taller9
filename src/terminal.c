#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#define TAMANO 100

int main(){

	printf("$ ");
	char salida[] = "exit";
	char entrada[TAMANO] = {0};
	fgets(entrada, sizeof(entrada), stdin);
	entrada[strcspn(entrada,"\n")] = 0;
	while(strcmp(entrada, salida) != 0){

		pid_t pid = fork();

		//PROCESO HIJO
		if(pid == 0){
			execl(entrada, entrada, (void *)0);
			perror("Error en el execl");
			printf("\n");
			exit(0);
		}
		//PROCESO PADRE
		else{
			wait(NULL);
			printf("$ ");
			fgets(entrada, sizeof(entrada), stdin);
			entrada[strcspn(entrada,"\n")] = 0;
		}
	}
	return 0;

}

