bin/terminal: obj/terminal.o
	gcc obj/terminal.o -o bin/terminal

bin/hola: obj/hola.o
	gcc obj/hola.o -o bin/hola

obj/terminal.o: src/terminal.c
	gcc -Wall -c src/terminal.c -o obj/terminal.o

obj/hola.o: src/hola.c
	        gcc -Wall -c src/hola.c -o obj/hola.o

.PHONY:clean

clean:
	rm -rf obj bin
	mkdir obj bin
